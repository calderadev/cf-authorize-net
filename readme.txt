=== Authroize.net for Caldera Forms ===
Contributors:      Shelob9, Desertsnowman
Donate link:       https://calderawp.com
Tags:              calderawp, caldera forms, wpform, form, responsive
Requires at least: 4.0
Tested up to:      4.3
Stable tag: 2.1.3.1
License:           GPLv2 or later
License URI:       http://www.gnu.org/licenses/gpl-2.0.html

Accept payments and credit cards via Authorize.net

== Description ==



== Installation ==

= Manual Installation =

1. Upload the entire `/authroize.net-for-caldera-forms` directory to the `/wp-content/plugins/` directory.
2. Activate Authroize.net for Caldera Forms through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1.0 =
* First release

== Upgrade Notice ==

= 0.1.0 =
First Release
