<?php
/**
 * Plugin Name: Authorize.net for Caldera Forms
 * Plugin URI:  https://calderaforms.com/downloads/
 * Description: Accept payments and credit cards via Authorize.net
 * Version: 2.1.3.1
 * Author:      Josh Pollock for CalderaWP LLC
 * Author URI:  https://CalderaWP.com
 * License:     GPLv2+
 * Text Domain: cf-authorize_net
 * Domain Path: /languages
 */

/**
 * Copyright (c) 2015 Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com) for CalderaWP LLC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Define constants
 */
define( 'CF_AUTHORIZE_NET_VER', '2.1.2' );
define( 'CF_AUTHORIZE_NET_URL',     plugin_dir_url( __FILE__ ) );
define( 'CF_AUTHORIZE_NET_PATH',    dirname( __FILE__ ) . '/' );
define( 'CF_AUTHORIZE_NET_CORE',    dirname( __FILE__ )  );
define( 'CF_AUTHORIZE_NET_BASENAME', plugin_basename( __FILE__ ) );

/**
 * Default initialization for the plugin:
 * - Registers the default textdomain.
 */
function cf_authorize_net_init_text_domain() {
	load_plugin_textdomain( 'cf-authorize_net', FALSE, CF_AUTHORIZE_NET_PATH . 'languages' );
}

/**
 * Include Files
 */
// load dependencies
include_once CF_AUTHORIZE_NET_PATH . 'vendor/autoload.php';

// pull in the functions file
include CF_AUTHORIZE_NET_PATH . 'includes/functions.php';

include  CF_AUTHORIZE_NET_PATH  .'includes/fields.php';

include CF_AUTHORIZE_NET_PATH . 'includes/v1-compat.php';

/**
 * Hooks
 */
//register text domain
add_action( 'init', 'cf_authorize_net_init_text_domain' );

// Load addon -- version 1 (leave this for backwards compat)
add_filter('caldera_forms_get_form_processors', 'cf_authorize_net_register');
//Load add-on -- version 2
add_action( 'caldera_forms_pre_load_processors', 'cf_authorize_net_load' );


// filter to initialize the license system
add_action( 'admin_init', 'cf_authorize_net_init_license' );

//add example forms
add_filter( 'caldera_forms_get_form_templates', 'cf_authorize_net_example_form' );

//add activation hook
add_action( 'activate_' . CF_AUTHORIZE_NET_BASENAME,  'cf_authorize_net_activate' );



/**
 * Plugin activation callback.
 */
function cf_authorize_net_activate(){

};
