<?php

abstract  class CF_Auth_Net_AIM extends CF_Auth_Net_Payment {



	/**
	 * Try to do transaction via remote API
	 *
	 * @since 2.0.0
	 *
	 * @param object $transaction
	 * @param bool $auth_only Optional. If true, payment is authorized, but not captured. If false, the default, it is authorized and captured.
	 */
	protected function try_transaction(  $transaction, $auth_only = false ){

		try {
			if( false == $auth_only ) {
				$response = $transaction->authorizeAndCapture();

			}else{
				$response = $transaction->authorizeOnly();
			}
			$this->response( $response );
		} catch ( AuthorizeNetException $e ) {
			$this->data_object->add_error( 'request_error', $e->getMessage() );

		}

	}

	/**
	 * Handle response from remote API, adding errors if not approved
	 *
	 * @since 2.0.0
	 *
	 * @param AuthorizeNetAIM_Response $response
	 */
	protected function response( AuthorizeNetAIM_Response $response ){

		if ( ! $response->approved ) {
			if ( isset( $response->response_reason_text ) ) {
				$this->data_object->add_error( $response->response_reason_text );
			}

			if ( isset( $response->error_message ) ) {
				$this->data_object->add_error( $response->error_message );
			}

		}else{

			$this->setup_transata( $this->processid );
			global $transdata;
			$transdata[ $this->processid ][ 'transaction_data' ] = $response;
		}

	}

	/**
	 * Setup customer object
	 *
	 * @since 2.0.0
	 *
	 * @param AuthorizeNetAIM $transaction
	 * @param string $prefix
	 *
	 * @return AuthorizeNetAIM
	 */
	protected function set_up_customer( AuthorizeNetAIM $transaction, $prefix ){
		$customer = new stdClass();
		$customer->first_name = $this->data_object->get_value( $prefix .  'first_name' );
		$customer->last_name = $this->data_object->get_value( $prefix .  'last_name' );
		$customer->address = $this->data_object->get_value( $prefix .  'card_address' );
		$customer->city = $this->data_object->get_value( $prefix .  'card_city' );
		$customer->state = $this->data_object->get_value( $prefix .  'card_state' );
		$customer->zip = $this->data_object->get_value( $prefix .  'card_zip' );
		$customer->country = $this->data_object->get_value( $prefix .  'card_country' );
		$customer->email = $this->data_object->get_value( $prefix .  'user_email' );
		$customer->customer_ip = cf_authorize_net_get_ip();
		$customer->phone = $this->data_object->get_value( $prefix . 'phone' );
		$customer->customer_ip = cf_authorize_net_get_ip();
		$customer->company = $this->data_object->get_value( $prefix . 'company' );

		/**
		 * Change customer data before sending to Authorize.net API
		 *
		 * @since 2.0.0
		 *
		 * @param \stdClass $customer Customer data as a stdClass object
		 * @param string $prefix Prefix used for data object
		 * @param \Caldera_Forms_Processor_Get_Data $data_object Processor data.
		 */
		$customer = apply_filters( 'cf_authorize_net_setup_customer', $customer, $prefix, $this->data_object );
		$transaction->setFields( (array) $customer );
		return $transaction;

	}


}
