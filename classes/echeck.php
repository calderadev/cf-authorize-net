<?php

class CF_Auth_Net_ECheck extends CF_Auth_Net_AIM {

	protected $prefix = 'auth-net-echeck-';

	/**
	 * @inheritdoc
	 */
	public function do_payment( array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object ) {
		$transaction = new AuthorizeNetAIM( $this->data_object->get_value( $this->prefix . 'api_login' ), $this->data_object->get_value( $this->prefix . 'transaction_key' ));
		$transaction->setSandbox( $this->is_sandbox() );

		$transaction->amount = $data_object->get_value( $this->prefix . 'amount' );

		$transaction->setECheck(
			$data_object->get_value( $this->prefix . 'bank_aba_code' ),
			$data_object->get_value( $this->prefix . 'bank_acct_num' ),
			$data_object->get_value( $this->prefix . 'bank_acct_type' ),
			$data_object->get_value( $this->prefix . 'bank_name' ),
			$data_object->get_value( $this->prefix . 'bank_acct_name' ),
			'WEB'
		);


		$transaction->invoice_num = $this->data_object->get_value( $this->prefix . 'invoice_num' );
		$transaction->description = $this->data_object->get_value( $this->prefix . 'description' );

		$this->set_up_customer( $transaction, $this->prefix );
		$this->try_transaction( $transaction );
		return $this->data_object;


	}
}