<?php


abstract class CF_Auth_Net_Payment extends Caldera_Forms_Processor_Payment {

	private $sandbox;

	protected $prefix;

	protected $processid;

	/**
	 * @inheritdoc
	 */
	public function pre_processor( array $config, array $form, $proccesid ) {
		$this->processid = $proccesid;

		if ( 'on' !=  $config[ $this->prefix . 'sandbox' ]  && ! is_ssl() ) {
			return array(
				'note' => __( 'Payment could not be processed as this site is not using HTTPS', 'cf-authorize-net' ),
				'type' => 'error'
			);

		}

		$this->set_data_object_initial( $config, $form  );
		$this->set_sandbox();


		$this->data_object = $this->do_payment( $config, $form, $proccesid, $this->data_object );


		$errors = $this->data_object->get_errors();
		if( ! empty( $errors ) ){
			return $errors;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function processor( array $config, array $form, $proccesid ) {
		$this->processid = $proccesid;
		global  $transdata;
		$this->set_data_object_from_transdata( $proccesid );

		$data = $this->data_object->get_values();
		$fields = $this->data_object->get_fields();
		$this->hash_field_values( $config, $form, $data, $fields );

		if ( ! isset( $transdata[ $proccesid ][ 'meta' ] ) ) {
			$transdata[ $proccesid ][ 'meta' ] = array();
		}

		if ( isset($transdata[ $proccesid ][ 'transaction_data' ] ) ) {

			foreach ( array( 'transaction_id', 'auth_capture', 'auth_capture', 'customer_id' ) as $field ) {
				if( isset( $transdata[ $proccesid ][ 'transaction_data' ] ->$field ) ){
					$transdata[ $proccesid ][ 'meta' ][ $field ] = $transdata[ $proccesid ][ 'transaction_data' ] ->$field;
				}
			}

			if(  isset($transdata[ $proccesid ][ 'transaction_data' ]->subscriptionId ) ){
				$transdata[ $proccesid ][ 'meta' ][ 'subscriptionId' ] = $transdata[ $proccesid ][ 'transaction_data' ]->subscriptionId;
			}

			if(  isset($transdata[ $proccesid ][ 'transaction_data' ]->profile ) ){
				foreach( $transdata[ $proccesid ][ 'transaction_data' ]->profile  as $field => $value ){
					$transdata[ $proccesid ][ 'meta' ][ $field ] = $value;
				}
			}

		}



		return $transdata[ $proccesid ][ 'meta' ];

	}

	/**
	 * @param array $config
	 * @param array $form
	 * @param $data
	 * @param $fields
	 *
	 * @return mixed
	 */
	protected function hash_field_values( array $config, array $form, $data, $fields ) {
		foreach( $fields  as $field ){
			if( isset( $field[ 'hash' ] ) && $field[ 'hash' ] ){
				$field_id = $field[ 'config_field' ];
				Caldera_Forms::set_field_data( $field_id, 'XXXXXX', $form );
			}
		}



	}

	/**
	 * Util function to check for sandbox mode.
	 *
	 * @since 2.0.1
	 *
	 * @return bool
	 */
	protected function is_sandbox(){
		return $this->sandbox;
	}

	/**
	 * Set sandbox property
	 *
	 * @since 2.0.1
	 *
	 */
	private function set_sandbox(  ){
		$this->sandbox = (bool) $this->data_object->get_value( $this->prefix . 'sandbox' );
	}


}