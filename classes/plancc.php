<?php
/**
 * Handles recurring payment plans via Credit Card
 *
 * @package cf
 * Copyright 2017 Josh Pollock <Josh@CalderaWP.com
 */

/**
 * Class CF_Auth_Net_RecCC
 *
 * Copyright 2017 CalderaWP LLCJosh Pollock <Josh@CalderaWP.com
 */
class CF_Auth_Net_Plancc extends CF_Auth_Net_Plan {


	protected $prefix = 'auth-net-rec-';

	/** @inheritdoc */
	protected function payment( Caldera_Forms_Processor_Get_Data $data_object ) {
		$creditCard = new \net\authorize\api\contract\v1\CreditCardType();
		$creditCard->setCardNumber( $data_object->get_value( $this->prefix . 'card_number' ) );
		$year = (string) $data_object->get_value( $this->prefix . 'card_exp_year' );
		if ( 2 == strlen( $year ) ) {
			$year = '20' . $year;
		}

		$creditCard->setExpirationDate( $year . '-' . $data_object->get_value( $this->prefix . 'card_exp_month' ) );
		$creditCard->setCardCode( $data_object->get_value( $this->prefix . 'card_cvc' ) );

		$payment = new \net\authorize\api\contract\v1\PaymentType();
		$payment->setCreditCard( $creditCard );

		return $payment;
	}


}