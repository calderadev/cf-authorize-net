<?php

class CF_Auth_Net_Single extends CF_Auth_Net_AIM {

	protected $prefix = 'auth-net-single-';

	/**
	 * @inheritdoc
	 */
	public function do_payment( array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object ) {

		$transaction = new AuthorizeNetAIM( $this->data_object->get_value( $this->prefix . 'api_login' ), $this->data_object->get_value( $this->prefix . 'transaction_key' ));
		$transaction->amount = $this->data_object->get_value( $this->prefix . 'price' );
		$transaction->card_num = $this->data_object->get_value( $this->prefix . 'card_number' );
		$transaction->exp_date = $this->data_object->get_value( $this->prefix . 'card_exp_month' ) . '/' .  $this->data_object->get_value( $this->prefix . 'card_exp_year' );
		$transaction->card_code = $this->data_object->get_value( $this->prefix . 'card_cvc' );
		$transaction->invoice_num = $this->data_object->get_value( $this->prefix . 'invoice_num' );
		$transaction->description = $this->data_object->get_value( $this->prefix . 'description' );

		$transaction->setSandbox( $this->is_sandbox() );

		$auth_only = false;
		$_auth_only = $this->data_object->get_value( $this->prefix . 'auth_only');
		if( $_auth_only && true == $_auth_only  ){
			$auth_only = true;
		}

		$this->set_up_customer( $transaction, $this->prefix );
		$this->try_transaction( $transaction, $auth_only );
		return $this->data_object;

	}


}