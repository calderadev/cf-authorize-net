<?php


abstract class CF_Auth_Net_Plan extends CF_Auth_Net_Payment{


	/**
	 * Form config
	 *
	 * @since 2.1.0
	 *
	 * @var array
	 */
	protected $form;

	/**
	 * @inheritdoc
	 */
	public function do_payment( array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object ) {

		$this->form = $form;
		$merchantAuthentication = $this->merchant_authentication( $data_object );

		// Subscription Type Info
		$subscription = $this->subscription( $data_object );

		return $this->request( $data_object, $merchantAuthentication, $subscription );

	}


	/**
	 * Make API request
	 *
	 * @since 2.1.0
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 * @param  \net\authorize\api\contract\v1\MerchantAuthenticationType $merchantAuthentication
	 * @param  \net\authorize\api\contract\v1\ARBSubscriptionType $subscription
	 *
	 * @return Caldera_Forms_Processor_Get_Data
	 */
	protected function request( Caldera_Forms_Processor_Get_Data $data_object, $merchantAuthentication, $subscription ) {
		global $transdata;
		/**
		 * Change reference ID
		 *
		 * @since 2.1.0
		 *
		 * @param string $refId Reference ID to send
		 * @param Caldera_Forms_Processor_Get_Data $data_object
		 * @param array $form Form config
		 */
		$refId   = apply_filters( 'cf_auth_net_ref_id', 'ref' . time(),  $this->data_object, $this->form );
		$request = new \net\authorize\api\contract\v1\ARBCreateSubscriptionRequest();
		$request->setmerchantAuthentication( $merchantAuthentication );
		$request->setRefId( $refId );
		$request->setSubscription( $subscription );

		$controller = new \net\authorize\api\controller\ARBCreateSubscriptionController( $request );

		if ( $this->is_sandbox() ) {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX );
		} else {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION );
		}

		if ( ( $response != null ) && ( $response->getMessages()->getResultCode() == "Ok" ) ) {
			$this->setup_transata( $this->processid );
			global $transdata;
			$transdata[ $this->processid ][ 'transaction_data' ] = $response;

			return $data_object;
		} else {
			$messages = $response->getMessages()->getMessage();


				foreach ( $messages as $message ) {
					$error = $message->getText();
					if ( ! empty( $error) ) {
						$data_object->add_error( $error );
					}else{
						$data_object->add_error( __( 'Error' ) );
					}
				}



			return $data_object;

		}

	}

	/**
	 * Create merchant authorization object
	 *
	 * @since 2.1.0
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 *
	 * @return \net\authorize\api\contract\v1\MerchantAuthenticationType
	 */
	protected function merchant_authentication( Caldera_Forms_Processor_Get_Data $data_object ) {
		$merchantAuthentication = new \net\authorize\api\contract\v1\MerchantAuthenticationType();
		$merchantAuthentication->setName( $data_object->get_value( $this->prefix . 'api_login' ) );
		$merchantAuthentication->setTransactionKey( $data_object->get_value( $this->prefix . 'transaction_key' ) );

		return $merchantAuthentication;
	}

	/**
	 * Create payment object
	 *
	 * @since 2.1.0
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 *
	 * @return \net\authorize\api\contract\v1\PaymentType
	 */
	abstract protected function payment( Caldera_Forms_Processor_Get_Data $data_object );

	/**
	 * Create bill to object
	 *
	 * @since 2.1.0
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 *
	 * @return \net\authorize\api\contract\v1\NameAndAddressType
	 */
	protected function bill_to( Caldera_Forms_Processor_Get_Data $data_object ) {
		$billTo = new \net\authorize\api\contract\v1\NameAndAddressType();
		$address = $data_object->get_value( $this->prefix . 'card_address' );
		$address_2 = $data_object->get_value( $this->prefix . 'card_address_2' );
		if( ! empty( $address_2 ) ) {
			$address = $address . "\n" . $address_2;
		}
		$billTo
			->setFirstName( $data_object->get_value( $this->prefix . 'first_name' ) )
			->setLastName( $data_object->get_value( $this->prefix . 'last_name' ) )
			->setAddress( $address )
			->setCity( $data_object->get_value( $this->prefix . 'card_city'  ) )
			->setState( $data_object->get_value( $this->prefix . 'card_state'  ) )
			->setCountry( $data_object->get_value( $this->prefix . 'card_country'  ) )
			->setCompany( $data_object->get_value( $this->prefix . 'company' ) );
		return $billTo;
	}

	/**
	 * Create subscription onjkect
	 *
	 * @since 2.1.0
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 *
	 * @return \net\authorize\api\contract\v1\ARBSubscriptionType
	 */
	protected function subscription( Caldera_Forms_Processor_Get_Data $data_object ) {
		$subscription = new \net\authorize\api\contract\v1\ARBSubscriptionType();
		$subscription->setName( $data_object->get_value( $this->prefix . 'name' ) );
		$interval = new \net\authorize\api\contract\v1\PaymentScheduleType\IntervalAType();
		$interval->setLength( $data_object->get_value( $this->prefix . 'billing-interval' ) );
		$interval->setUnit( $data_object->get_value( $this->prefix . 'billing-units' ) );

		/**
		 * Modify interval object for Authorize.net ARB billing
		 *
		 * @since 2.1.0
		 *
		 * @param \net\authorize\api\contract\v1\PaymentScheduleType\IntervalAType $type
		 * @param Caldera_Forms_Processor_Get_Data $data_object
		 * @param array $form
		 */
		$interval = apply_filters( 'cf_auth_net_arb_interval', $interval, $data_object, $this->form );

		$paymentSchedule = $this->create_payment_schedule( $data_object, $interval );

		$subscription->setPaymentSchedule( $paymentSchedule );
		$subscription->setAmount( $data_object->get_value( $this->prefix . 'amount' ) );

		$trial_amount = $data_object->get_value( $this->prefix . 'trials-amount' );
		if ( null == $trial_amount ) {
			$trial_amount = "0.01";
		}
		$subscription->setTrialAmount( $trial_amount );

		$payment = $this->payment( $data_object );
		$subscription->setPayment( $payment );

		$billTo = $this->bill_to( $data_object );

		$subscription->setCustomer( $this->create_customer( $data_object ) );

		$subscription->setBillTo( $billTo );

		/**
		 * Modify subscription object for Authorize.net ARB billing
		 *
		 * @since 2.1.0
		 *
		 * @param \net\authorize\api\contract\v1\ARBSubscriptionType $subscription
		 * @param Caldera_Forms_Processor_Get_Data $data_object
		 * @param array $form
		 */
		return apply_filters( 'cf_auth_net_arb_subscription', $subscription, $data_object, $this->form );

	}


	/**
	 * Create payment schedule object
	 *
	 * @since 2.1.0
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 * @param \net\authorize\api\contract\v1\PaymentScheduleType\IntervalAType $interval
	 *
	 * @return \net\authorize\api\contract\v1\PaymentScheduleType
	 */
	protected function create_payment_schedule( Caldera_Forms_Processor_Get_Data $data_object, $interval ) {
		$paymentSchedule = new \net\authorize\api\contract\v1\PaymentScheduleType();

		$paymentSchedule->setInterval( $interval );
		$paymentSchedule->setStartDate( new DateTime( 'now' ) );
		$paymentSchedule->setTotalOccurrences( $data_object->get_value( $this->prefix . 'occurrences' ) );

		$trial_occurances = $data_object->get_value( $this->prefix . 'trial-occurrences' );
		if ( null == $trial_occurances || 0 === $trial_occurances || '0' === $trial_occurances ) {
			$trial_occurances = '0';
		}
		$trial_occurances = (string) $trial_occurances;

		$paymentSchedule->setTrialOccurrences( $trial_occurances );


		/**
		 * Modify payment schedule object for Authorize.net ARB billing
		 *
		 * @since 2.1.0
		 *
		 * @param \net\authorize\api\contract\v1\PaymentScheduleType $subscription
		 * @param Caldera_Forms_Processor_Get_Data $data_object
		 * @param array $form
		 */
		return apply_filters( 'cf_auth_net_arb_payment_schedule', $paymentSchedule, $data_object, $this->form );
	}

	/**
	 * Create a customer
	 *
	 * @since 2.1.3
	 *
	 * @param Caldera_Forms_Processor_Get_Data $data_object
	 * @return \net\authorize\api\contract\v1\CustomerType
	 */
	protected function create_customer( Caldera_Forms_Processor_Get_Data $data_object ){
		$customer = new \net\authorize\api\contract\v1\CustomerType();
		$customer
			->setEmail( $data_object->get_value( $this->prefix . 'user_email' ) );
		return $customer;
	}
}

