<?php

/**
 * Class CF_Auth_Net_Planecheck
 *
 * Reucrring payments via eChecks
 */
class CF_Auth_Net_Planecheck  extends CF_Auth_Net_Plan {

	/** @inheritdoc */
	protected $prefix = 'auth-net-rec-echeck-';

	/** @inheritdoc */
	protected function payment( Caldera_Forms_Processor_Get_Data $data_object ) {

		$payment = new \net\authorize\api\contract\v1\PaymentType();

		$bankAccount = new \net\authorize\api\contract\v1\BankAccountType();
		$bankAccount->setBankName( $data_object->get_value( $this->prefix . 'bank_name' ) );
		$bankAccount->setNameOnAccount(  $data_object->get_value( $this->prefix . 'bank_acct_name' ) );
		$bankAccount->setRoutingNumber( $data_object->get_value( $this->prefix . 'bank_aba_code' ) );
		$bankAccount->setAccountNumber( $data_object->get_value( $this->prefix . 'bank_acct_num'  ) );
		//$bankAccount->setAccountType( $data_object->get_value( $this->prefix . 'bank_acct_type' )  );
		$bankAccount->setEcheckType( 'WEB' );


		$payment->setBankAccount( $bankAccount );

		return $payment;
	}

}