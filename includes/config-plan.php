<?php
/**
 * Processor config UI for Authorize.net for Caldera Forms payment plans
 *
 * @package   cf_authorize-net
 * @author    Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com)
 * @license   GPL-2.0+
 * @link
 * @copyright 2016 Josh Pollock for CalderaWP LLC
 */

if ( class_exists( 'Caldera_Forms_Processor_UI' ) ) {
	echo Caldera_Forms_Processor_UI::ssl_notice( 'Authorize.net for Caldera Forms' );
	echo Caldera_Forms_Processor_UI::config_fields( cf_authorize_net_fields_plan() );
}
