<?php


/**
 * Registers the old Authorize.net for Caldera Forms Processor
 *
 * @uses "caldera_forms_get_form_processors" filter
 *
 * @since 0.1.0
 * @param array		$processors		Array of current registered processors
 *
 * @return array	Array of registered processors
 */
function cf_authorize_net_register($processors){
	if ( ! class_exists( 'Caldera_Forms_Processor_Get_Data' ) ) {
		return $processors;

	}

	$processors['cf_authorize-net'] = array(
		"name"				=>	__( 'Authorize.net Version 1 (Deprecated)', 'cf-authorize-net'),
		"description"		=>	__( 'Please switch to the new processor.', 'cf-authorize-net'),
		"icon"				=>	CF_AUTHORIZE_NET_URL . "icon.png",
		"author"			=>	'Josh Pollock for CalderaWP LLC',
		"author_url"		=>	'https://CalderaWP.com',
		"pre_processor"		=>	'cf_authorize_net_pre_process',
		"processor"			=>  'cf_authorize_net_process',
		"template"			=>	CF_AUTHORIZE_NET_PATH . "includes/config.php",
		"cf_ver"            =>  "1.2.4"
	);

	return $processors;

}

/**
 * Pre-Process for old authorize.net for Caldera Forms processor
 *
 * @since 0.1.0
 *
 * @since 0.1.0
 *
 * @param array $config Processor config
 * @param array $form Form config
 * @param string $proccesid Unique ID for this instance of the processor.
 *
 * @return array
 */
function cf_authorize_net_pre_process( $config, $form, $proccesid ) {
	$processor_data = new Caldera_Forms_Processor_Get_Data( $config, $form, cf_authorize_net_fields() );
	if ( empty(  $config[ 'sandbox' ]  ) && ! is_ssl() ) {
		return array(
			'note' => __( 'Payment could not be processed as this site is not using HTTPS', 'cf-authourize-net' ),
			'type' => 'error'
		);

	}

	require_once( dirname( __FILE__ ) . '/sdk-php-master/autoload.php');
	global $transdata;


	$errors = $processor_data->get_errors();
	if ( ! empty( $errors ) ) {
		return $errors;

	}

	$processor_data = cf_authorize_net_do_cc_payment( $processor_data, $proccesid );
	$transdata[ $proccesid ][ 'object' ] = $processor_data;

	$errors = $processor_data->get_errors();
	if ( ! empty( $errors ) ) {
		return $errors;

	}


}


/**
 * Proccess for old Authorize.net for Caldera Forms proccessor
 *
 * @since 0.1.0
 *
 * @since 0.1.0
 *
 * @param array $config Processor config
 * @param array $form Form config
 * @param string $proccesid Unique ID for this instance of the processor.
 *
 * @return array
 */
function cf_authorize_net_process( $config, $form, $proccesid ) {
	global $transdata;

	$processor_data = $transdata[ $proccesid ][ 'object' ];
	$data = $processor_data->get_values();
	$fields = $processor_data->get_fields();
	if ( isset( $config[ 'card_number' ] ) && ! empty( $config[ 'card_number' ] ) ) {

		$number = $data[ 'card_number' ];
		$number = $number = substr( $number, 0, 4 ) . str_repeat( 'X', strlen( $number ) - 4 );
		$field = $fields[ 'card_number' ][ 'config_field' ];
		if ( $field ) {
			Caldera_Forms::set_field_data( $field, $number, $form );
		}
	}

	if ( isset( $config[ 'card_cvc'] ) && ! empty( $config[ 'card_cvc'] ) ) {
		$number = str_repeat( 'X', strlen( $data[ 'card_cvc' ] ) );
		$field = $fields[ 'card_cvc' ][ 'config_field' ];
		if ( $field ) {
			Caldera_Forms::set_field_data( $field, $number, $form );
		}
	}

	if ( ! isset( $transdata[ $proccesid ][ 'meta' ] ) ) {
		$transdata[ $proccesid ][ 'meta' ] = array();
	}



	return $transdata[ $proccesid ][ 'meta' ];



}

/**
 * Do a credit card payment with old processor
 *
 * @param Caldera_Forms_Processor_Get_Data $processor_data
 * @param string $proccesid Unique ID for this instance of the processor.
 *
 * @return Caldera_Forms_Processor_Get_Data
 */
function cf_authorize_net_do_cc_payment( $processor_data, $proccesid ) {
	$purchase_data = $processor_data->get_values();
	$transaction = new AuthorizeNetAIM(
		$purchase_data[ 'api_login' ],
		$purchase_data[ 'api_key' ]
	);

	if ( $purchase_data[ 'sandbox' ] ) {
		$transaction->setSandbox( true );
	} else {
		$transaction->setSandbox( false );
	}


	$card_names = explode( ' ', $purchase_data['card_name'] );

	if ( is_array( $card_names ) && ! empty( $card_names[0] ) && ! empty( $card_names[1] ) ) {
		$first_name = $card_names[0];
		unset( $card_names[0] );
		$last_name = implode( ' ', $card_names );
	} else {

		$processor_data->add_error( __( 'Name on card could not be proccesed properly', 'cf-auth-net' ) );
		return $processor_data;

	}

	if ( empty( $purchase_data[ 'invoice_num' ] ) ) {
		$purchase_data[ 'invoice_num' ] = $proccesid;
	}


	$transaction->amount    = $purchase_data['price'];
	$transaction->card_num  = strip_tags( trim( $purchase_data['card_number'] ) );
	$transaction->card_code = strip_tags( trim( $purchase_data['card_cvc'] ) );
	$transaction->exp_date  = strip_tags( trim( $purchase_data['card_exp_month'] ) ) . '/' . strip_tags( trim( $purchase_data['card_exp_year'] ) );

	$transaction->description = $purchase_data['description' ];
	$transaction->first_name  = $first_name;
	$transaction->last_name   = $last_name;

	$transaction->address = $purchase_data['card_address'] . ' ' . $purchase_data['card_address_2'];
	$transaction->city    = $purchase_data['card_city'];
	$transaction->country = $purchase_data['card_country'];
	$transaction->state   = $purchase_data['card_state'];
	$transaction->zip     = $purchase_data['card_zip'];

	$transaction->customer_ip = cf_authorize_net_get_ip();
	$transaction->email       = $purchase_data['user_email'];
	$transaction->invoice_num = $purchase_data['invoice_num'];
	$transaction->merchantCustomerId = $purchase_data[ 'customer_id' ];

	try {

		$response = $transaction->authorizeAndCapture();

		if ( $response->approved ) {
			return $processor_data;

		} else {

			if ( isset( $response->response_reason_text ) ) {
				$error = $response->response_reason_text;
			} elseif ( isset( $response->error_message ) ) {
				$error = $response->error_message;
			} else {
				$error = '';
			}


			if ( strpos( strtolower( $error ), 'the credit card number is invalid' ) !== false ) {
				$processor_data->add_error( __( 'Your card number is invalid', 'give' ) );
			} elseif ( strpos( strtolower( $error ), 'this transaction has been declined' ) !== false ) {
				$processor_data->add_error( __( 'Your card has been declined', 'give' ) );
			} elseif ( isset( $response->response_reason_text ) ) {
				$processor_data->add_error( $response->response_reason_text );
			} elseif ( isset( $response->error_message ) ) {
				$processor_data->add_error( $response->error_message );
			} else {
				$processor_data->add_error( sprintf( __( 'An error occurred. Error data: %s', 'give' ), print_r( $response, true ) ) );
			}

			return $processor_data;
		}
	}
	catch ( AuthorizeNetException $e ) {
		$processor_data->add_error( 'request_error', $e->getMessage() );

		return $processor_data;
	}

}

/**
 * Fields for V1 processor
 *
 * @deprecated
 *
 * @return array
 */
function cf_authorize_net_fields( ) {
	//_deprecated_function( __FUNCTION__, '2.0.0', '' );
	return  array(

		array(
			'id' => 'customer_id',
			'label' => __( 'Customer ID', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
			'desc' => __( 'Optional. Will be generated automatically by Authorize.net if not set.', 'cf-authorize-net' )

		),
		array(
			'id'   => 'price',
			'label' => __( 'Product Price', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'description',
			'label' => __( 'Description', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'label' => __( 'Test Mode', 'cf-authorize-net' ),
			'id'   => 'sandbox',
			'type' => 'checkbox',
			'required' => false,
		),
		array(
			'label' => __( 'API Login', 'cf-authorize-net' ),
			'id'   => 'api_login',
			'type' => 'text',
			'required' => true,
			'magic' => false,
		),
		array(
			'id'   => 'api_key',
			'label' => __( 'Live Public Key', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
			'magic' => false,
		),
		array(
			'id'   => 'card_name',
			'label' => __( 'Name on Card', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
			'magic' => true,
		),
		array(
			'id'   => 'card_number',
			'label' => __( 'Card Number', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'card_cvc',
			'label' => __( 'CVC Code For Card', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'card_exp_month',
			'label' => __( 'Card Expiration Month', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'card_exp_year',
			'label' => __( 'Card Expiration Year', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'user_email',
			'label' => __( 'Customer Email', 'cf-authorize-net' ),
			'type' => 'advanced',
			'allow_types' => 'email',
			'required' => true,
		),
		array(
			'id'   => 'card_address',
			'label' => __( 'Address Line 1', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_address_2',
			'label' => __( 'Address Line 2', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_city',
			'label' => __( 'City', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_state',
			'label' => __( 'State', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_country',
			'label' => __( 'Country', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_zip',
			'label' => __( 'Zip', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'invoice_num',
			'label' => __( 'Invoice Number', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
			'desc' => __( 'If left empty a random number will be used', 'cf-authorize-net' )
		),
	);
}