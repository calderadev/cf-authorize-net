<?php
function cf_authorize_net_fields_single( ) {
	$fields = array(
		array(
			'id' => 'customer_id',
			'label' => __( 'Customer ID', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
			'desc' => __( 'Optional. Will be generated automatically by Authorize.net if not set.', 'cf-authorize-net' )

		),
		array(
			'id'   => 'price',
			'label' => __( 'Product Price', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'description',
			'label' => __( 'Description', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'auth-only',
			'label' => __( 'Authorize Only', 'cf-authorize-net' ),
			'type' => 'checkbox',
			'required' => false
		)
	);

	$fields = array_merge( cf_authorize_net_fields_basic(),  $fields );
	return cf_authorize_net_prefix_fields( 'auth-net-single-', $fields );
}

/**
 * Fields for recurring payments via credit card
 *
 * @since 2.1.0
 *
 * @return array
 */
function cf_authorize_net_fields_plan(){
	$fields = array_merge( cf_authorize_net_fields_basic(), cf_authorize_net_recurring_fields()  );
	return cf_authorize_net_prefix_fields( 'auth-net-rec-', $fields );

}

/**
 * Fields for recurring payments via eCheck
 *
 * @since 2.1.0
 *
 * @return array
 */
function cf_authorize_net_echeck_fields_plan(){

	$fields = array_merge(
		cf_authorize_net_api_fields(),
		cf_authorize_net_echeck_option_fields(),
		cf_authorize_net_recurring_fields()
	);
	return $fields = cf_authorize_net_prefix_fields( 'auth-net-rec-echeck-', $fields );

}



/**
 * @return array
 */
function cf_authorize_net_recurring_fields() {
	$fields = array(
		array(
			'label'    => __( 'Plan Name', 'cf-authorize-net' ),
			'id'       => 'plan_name',
			'required' => true,
		),
		array(
			'label'   => __( 'Length', 'cf-card_codeauthorize-net' ),
			'id'      => 'billing-units',
			'type'    => 'dropdown',
			'options' => array(
				'days'   => __( 'Days', 'cf-authorize-net' ),
				'weeks'  => __( 'Weeks', 'cf-authorize-net' ),
				'months' => __( 'Months', 'cf-authorize-net' ),
				'years'  => __( 'Years', 'cf-authorize-net' ),
			)
		),
		array(
			'label'    => __( 'Billing Interval', 'cf-authorize-net' ),
			'id'       => 'billing-interval',
			'required' => true,
		),
		array(
			'label'       => __( 'Occurrences', 'cf-authorize-net' ),
			'id'          => 'occurrences',
			'required'    => true,
			'description' => __( 'How many times customer will be billed', 'cf-authorize-net' ),
		),

		array(
			'label'       => __( 'Charge', 'cf-authorize-net' ),
			'id'          => 'amount',
			'required'    => true,
			'description' => __( 'How much the regular charge will be.', 'cf-authorize-net' ),
		),
		array(
			'label'       => __( 'Trial Charge', 'cf-authorize-net' ),
			'id'          => 'trials-amount',
			'required'    => true,
			'description' => __( 'How much the charge will be during trial period. Enter 0 for free trial.', 'cf-authorize-net' ),
		),
		array(
			'label'       => __( 'Trial Occurrences', 'cf-authorize-net' ),
			'id'          => 'trial-occurrences',
			'required'    => true,
			'description' => __( 'How many times customer will be billed during trial period', 'cf-authorize-net' ),
		),

	);

	return $fields;
}


function cf_authorize_net_fields_basic(){
	$fields = array(
		array(
			'id' => 'first_name',
			'label' => __( 'First Name', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'last_name',
			'label' => __( 'Last Name', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_number',
			'label' => __( 'Card Number', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
			'hash' => true
		),
		array(
			'id'   => 'card_cvc',
			'label' => __( 'CVC Code For Card', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
			'hash' => true
		),
		array(
			'id'   => 'card_exp_month',
			'label' => __( 'Card Expiration Month', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'card_exp_year',
			'label' => __( 'Card Expiration Year', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
		),
		array(
			'id'   => 'user_email',
			'label' => __( 'Customer Email', 'cf-authorize-net' ),
			'type' => 'advanced',
			'allow_types' => 'email',
			'required' => true,
		),
		array(
			'id'   => 'card_address',
			'label' => __( 'Address Line 1', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_address_2',
			'label' => __( 'Address Line 2', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_city',
			'label' => __( 'City', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_state',
			'label' => __( 'State', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_country',
			'label' => __( 'Country', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_zip',
			'label' => __( 'Zip', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'company',
			'label' => __( 'Customer Company Name', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'phone',
			'label' => __( 'Customer Phone number', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'invoice_num',
			'label' => __( 'Invoice Number', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
			'desc' => __( 'If left empty a random number will be used', 'cf-authorize-net' )
		),

	);

	return array_merge( cf_authorize_net_api_fields(), $fields );
}

function cf_authorize_net_echeck_fields(){
	$fields = 	$fields = array(
		array(
			'id'    => 'amount',
			'label' => __( 'Amount to charge', 'cf-authorize-net' ),
		),
	);

	$fields = array_merge(
		cf_authorize_net_api_fields(),
		$fields,
		cf_authorize_net_echeck_option_fields()
	);

	return cf_authorize_net_prefix_fields( 'auth-net-echeck-', $fields );
}

/**
 * Common fields for echeck single/ echeck recurring
 *
 * @since 2.1.0
 *
 * @return array
 */
function cf_authorize_net_echeck_option_fields() {
	$fields = array(
		array(
			'id' => 'first_name',
			'label' => __( 'First Name', 'cf-authorize-net' ),
			'description' => __( 'For billing, will show on receipt', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'last_name',
			'label' => __( 'Last Name', 'cf-authorize-net' ),
			'description' => __( 'For billing, will show on receipt', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		//JOSH - don't change card_ prefix or shit will break, deal with it.
		array(
			'id'   => 'card_address',
			'label' => __( 'Address Line 1', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_address_2',
			'label' => __( 'Address Line 2', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_city',
			'label' => __( 'City', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_state',
			'label' => __( 'State', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_country',
			'label' => __( 'Country', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'card_zip',
			'label' => __( 'Zip', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'company',
			'label' => __( 'Customer Company Name', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id' => 'phone',
			'label' => __( 'Customer Phone number', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => false,
		),
		array(
			'id'   => 'user_email',
			'label' => __( 'Customer Email', 'cf-authorize-net' ),
			'type' => 'advanced',
			'allow_types' => 'email',
			'required' => true,
		),
		array(
			'id'    => 'bank_aba_code',
			'label' => __( 'ABA routing transit number (ABA RTN)', 'cf-authorize-net' ),
			'hash'  => true
		),
		array(
			'id'    => 'bank_acct_num',
			'label' => __( 'Bank account number', 'cf-authorize-net' ),
			'hash'  => true
		),
		array(
			'id'    => 'bank_name',
			'label' => __( 'Bank name', 'cf-authorize-net' ),
			'hash'  => true
		),
		array(
			'id'    => 'bank_acct_name',
			'label' => __( 'Name on bank account.', 'cf-authorize-net' ),
			'hash'  => true
		),
		array(
			'id'      => 'bank_acct_type',
			'label'   => __( 'Account Type', 'cf-authorize-net' ),
			'type'    => 'dropdown',
			'options' => array(
				'CHECKING' => __( 'Checking', 'cf-authorize-net' ),
				'SAVINGS'  => __( 'Savings', 'cf-authorize-net' )
			)
		),
	);

	return $fields;
}

function cf_authorize_net_prefix_fields( $prefix, array $fields ){
	foreach( $fields  as &$field ){
		$field[ 'id' ] = $prefix . $field[ 'id' ];
	}
	
	return $fields;
	
}


function cf_authorize_net_api_fields(){
	return array(
		array(
			'label' => __( 'Sandbox Mode', 'cf-authorize-net' ),
			'id'   => 'sandbox',
			'type' => 'checkbox',
			'required' => false,
		),
		array(
			'label' => __( 'API Login', 'cf-authorize-net' ),
			'id'   => 'api_login',
			'type' => 'text',
			'required' => true,
			'magic' => false,
		),
		array(
			'id'   => 'transaction_key',
			'label' => __( 'Transaction Key', 'cf-authorize-net' ),
			'type' => 'text',
			'required' => true,
			'magic' => false,
		),
	);
}