<?php
return array(
	'_last_updated' => 'Fri, 27 Jan 2017 02:39:52 +0000',
	'ID' => 'echeck-plan',
	'cf_version' => '1.4.8',
	'name' => 'eCheck Plan',
	'description' => '																																										',
	'db_support' => 1,
	'pinned' => 0,
	'hide_form' => 1,
	'check_honey' => 1,
	'success' => 'Form has been successfully submitted. Thank you.																		',
	'avatar_field' => '',
	'form_ajax' => 1,
	'custom_callback' => '',
	'layout_grid' =>
		array(
			'fields' =>
				array(
					'fld_9668342' => '1:1',
					'fld_6816023' => '2:1',
					'fld_6573255' => '2:2',
					'fld_9267447' => '3:1',
					'fld_9756342' => '4:1',
					'fld_3367393' => '4:1',
					'fld_1050571' => '4:2',
					'fld_5917507' => '4:2',
					'fld_5786264' => '5:2',
				),
			'structure' => '12|6:6|12|6:6|6:6',
		),
	'fields' =>
		array(
			'fld_9668342' =>
				array(
					'ID' => 'fld_9668342',
					'type' => 'html',
					'label' => 'Signup Description',
					'slug' => 'signup_description',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'default' => '<h1>Make Online Payments Via Check</h1>
<p>12 $100 Monthly Payments</p>',
						),
				),
			'fld_6816023' =>
				array(
					'ID' => 'fld_6816023',
					'type' => 'text',
					'label' => 'First Name',
					'slug' => 'first_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_9267447' =>
				array(
					'ID' => 'fld_9267447',
					'type' => 'text',
					'label' => 'Email',
					'slug' => 'email',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_9756342' =>
				array(
					'ID' => 'fld_9756342',
					'type' => 'text',
					'label' => 'Routing Number',
					'slug' => 'routing_number_',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => 'ABA routing transit number ',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_3367393' =>
				array(
					'ID' => 'fld_3367393',
					'type' => 'text',
					'label' => 'Name On Account',
					'slug' => 'name_on_account',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_1050571' =>
				array(
					'ID' => 'fld_1050571',
					'type' => 'text',
					'label' => 'Bank Account Number',
					'slug' => 'bank_account_number',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => 'ABA Account number',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => 99,
						),
				),
			'fld_5917507' =>
				array(
					'ID' => 'fld_5917507',
					'type' => 'text',
					'label' => 'Bank Name',
					'slug' => 'bank_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_5786264' =>
				array(
					'ID' => 'fld_5786264',
					'type' => 'button',
					'label' => 'Pay',
					'slug' => 'pay',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'type' => 'submit',
							'class' => 'btn btn-default',
							'target' => '',
						),
				),
			'fld_6573255' =>
				array(
					'ID' => 'fld_6573255',
					'type' => 'text',
					'label' => 'Last Name',
					'slug' => 'last_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
		),
	'page_names' =>
		array(
			0 => 'Page 1',
		),
	'mailer' =>
		array(
			'on_insert' => 1,
			'sender_name' => 'Caldera Forms Notification',
			'sender_email' => 'admin@example.com',
			'reply_to' => '',
			'email_type' => 'html',
			'recipients' => '',
			'bcc_to' => '',
			'email_subject' => 'New Payment Plan Created',
			'email_message' => '{summary}',
		),
	'processors' =>
		array(
			'fp_97667047' =>
				array(
					'ID' => 'fp_97667047',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'increment_capture',
					'config' =>
						array(
							'start' => 1,
							'field' => '',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
			'fp_40867988' =>
				array(
					'ID' => 'fp_40867988',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'auth-new-plan-echeck',
					'config' =>
						array(
							'auth-net-rec-echeck-sandbox' => 'on',
							'auth-net-rec-echeck-api_login' => 'CHANGE ME!',
							'auth-net-rec-echeck-transaction_key' => 'CHANGE ME!',
							'auth-net-rec-echeck-first_name' => '%first_name%',
							'auth-net-rec-echeck-last_name' => '%last_name%',
							'auth-net-rec-echeck-bank_aba_code' => '%routing_number_%',
							'auth-net-rec-echeck-bank_acct_num' => '%bank_account_number%',
							'auth-net-rec-echeck-bank_name' => '%bank_name%',
							'auth-net-rec-echeck-bank_acct_name' => 'Josh Pollock',
							'auth-net-rec-echeck-bank_acct_type' => 'CHECKING',
							'auth-net-rec-echeck-plan_name' => '10 Dollars A Month For  A Year',
							'auth-net-rec-echeck-billing-units' => 'months',
							'auth-net-rec-echeck-billing-interval' => 1,
							'auth-net-rec-echeck-occurrences' => 11,
							'auth-net-rec-echeck-amount' => 100,
							'auth-net-rec-echeck-trials-amount' => 100,
							'auth-net-rec-echeck-trial-occurrences' => 1,
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
		),
	'conditional_groups' =>
		array(
			'_open_condition' => '',
		),
	'settings' =>
		array(
			'responsive' =>
				array(
					'break_point' => 'sm',
				),
		),
	'version' => '1.4.8',
);
