<?php
return  array(
	'_last_updated' => 'Fri, 19 Aug 2016 22:33:06 +0000',
	'ID' => 'a20',
	'cf_version' => '1.4.2-b1',
	'name' => 'Authorize.net - One Time Payment - Variable Pricing',
	'description' => '',
	'db_support' => 1,
	'pinned' => 0,
	'hide_form' => 1,
	'check_honey' => 1,
	'success' => 'Form has been successfully submitted. Thank you.',
	'avatar_field' => '',
	'form_ajax' => 1,
	'custom_callback' => '',
	'layout_grid' =>
		array(
			'fields' =>
				array(
					'fld_4859634' => '1:1',
					'fld_8160155' => '1:2',
					'fld_6816023' => '2:1',
					'fld_9267447' => '2:2',
					'fld_128749' => '3:1',
					'fld_7758358' => '3:1',
					'fld_9516739' => '3:1',
					'fld_8804140' => '3:1',
					'fld_9380587' => '4:1',
					'fld_5892727' => '4:2',
					'fld_9387333' => '5:1',
					'fld_8436903' => '5:2',
					'fld_7740244' => '6:1',
					'fld_9756342' => '7:1',
					'fld_1050571' => '7:2',
					'fld_2715295' => '7:3',
					'fld_3712349' => '8:1',
					'fld_2662828' => '8:1',
					'fld_730490' => '8:1',
					'fld_5786264' => '8:2',
				),
			'structure' => '6:6|6:6|12|6:6|6:6|12|4:4:4|6:6',
		),
	'fields' =>
		array(
			'fld_4859634' =>
				array(
					'ID' => 'fld_4859634',
					'type' => 'dropdown',
					'label' => 'Price',
					'slug' => 'price',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'auto_type' => '',
							'taxonomy' => 'category',
							'post_type' => 'post',
							'value_field' => 'name',
							'orderby_tax' => 'name',
							'orderby_post' => 'name',
							'order' => 'ASC',
							'show_values' => 1,
							'option' =>
								array(
									'opt1577174' =>
										array(
											'value' => 1,
											'label' => 'One Dollar Thing',
										),
									'opt1163870' =>
										array(
											'value' => 2,
											'label' => 'Two Dollar Thing',
										),
								),
							'default' => 'opt1163870',
						),
				),
			'fld_8160155' =>
				array(
					'ID' => 'fld_8160155',
					'type' => 'dropdown',
					'label' => 'Quantity',
					'slug' => 'quantity',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'auto_type' => '',
							'taxonomy' => 'category',
							'post_type' => 'post',
							'value_field' => 'name',
							'orderby_tax' => 'name',
							'orderby_post' => 'name',
							'order' => 'ASC',
							'show_values' => 1,
							'option' =>
								array(
									'opt1456570' =>
										array(
											'value' => 1,
											'label' => '1 Things',
										),
									'opt1668097' =>
										array(
											'value' => 2,
											'label' => '2 Things',
										),
									'opt1797882' =>
										array(
											'value' => 3,
											'label' => '3 Things',
										),
								),
							'default' => 'opt1668097',
						),
				),
			'fld_6816023' =>
				array(
					'ID' => 'fld_6816023',
					'type' => 'text',
					'label' => 'First Name',
					'slug' => 'first_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_128749' =>
				array(
					'ID' => 'fld_128749',
					'type' => 'email',
					'label' => 'Email Address',
					'slug' => 'email_address',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
						),
				),
			'fld_7758358' =>
				array(
					'ID' => 'fld_7758358',
					'type' => 'text',
					'label' => 'Company',
					'slug' => 'company',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9516739' =>
				array(
					'ID' => 'fld_9516739',
					'type' => 'text',
					'label' => 'Address',
					'slug' => 'address',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_8804140' =>
				array(
					'ID' => 'fld_8804140',
					'type' => 'text',
					'label' => 'Address Line Two',
					'slug' => 'address_line_two',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9380587' =>
				array(
					'ID' => 'fld_9380587',
					'type' => 'states',
					'label' => 'State/ Province',
					'slug' => 'state_province',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
						),
				),
			'fld_5892727' =>
				array(
					'ID' => 'fld_5892727',
					'type' => 'text',
					'label' => 'City',
					'slug' => 'city',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9387333' =>
				array(
					'ID' => 'fld_9387333',
					'type' => 'text',
					'label' => 'Postal Code',
					'slug' => 'postal_code',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_8436903' =>
				array(
					'ID' => 'fld_8436903',
					'type' => 'dropdown',
					'label' => 'Country',
					'slug' => 'country',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'auto_type' => '',
							'taxonomy' => 'category',
							'post_type' => 'post',
							'value_field' => 'name',
							'orderby_tax' => 'name',
							'orderby_post' => 'name',
							'order' => 'ASC',
							'show_values' => 1,
							'default' => 'opt1192667',
							'option' =>
								array(
									'opt1192667' =>
										array(
											'value' => 'usa',
											'label' => 'United States',
										),
									'opt1677744' =>
										array(
											'value' => 'canada',
											'label' => 'Canada',
										),
								),
						),
				),
			'fld_7740244' =>
				array(
					'ID' => 'fld_7740244',
					'type' => 'text',
					'label' => 'Credit Card Number',
					'slug' => 'credit_card_number',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => 4007000000027,
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9756342' =>
				array(
					'ID' => 'fld_9756342',
					'type' => 'text',
					'label' => 'Expiration Month',
					'slug' => 'expiration_month',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'masked' => 1,
							'mask' => 99,
							'type_override' => 'text',
						),
				),
			'fld_1050571' =>
				array(
					'ID' => 'fld_1050571',
					'type' => 'text',
					'label' => 'Expiration Year',
					'slug' => 'expiration_year',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'masked' => 1,
							'mask' => 99,
							'type_override' => 'text',
						),
				),
			'fld_2715295' =>
				array(
					'ID' => 'fld_2715295',
					'type' => 'text',
					'label' => 'Secret Code',
					'slug' => 'secret_code',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_3712349' =>
				array(
					'ID' => 'fld_3712349',
					'type' => 'calculation',
					'label' => 'Total',
					'slug' => 'total',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'element' => 'h3',
							'classes' => 'total-line',
							'before' => 'Total:',
							'after' => '',
							'fixed' => 1,
							'thousand_separator' => ',',
							'formular' => ' ( fld_8160155*fld_4859634 ) ',
							'config' =>
								array(
									'group' =>
										array(
											0 =>
												array(
													'lines' =>
														array(
															0 =>
																array(
																	'operator' => '+',
																	'field' => 'fld_8160155',
																),
															1 =>
																array(
																	'operator' => '*',
																	'field' => 'fld_4859634',
																),
														),
												),
										),
								),
							'manual_formula' => '',
						),
				),
			'fld_2662828' =>
				array(
					'ID' => 'fld_2662828',
					'type' => 'hidden',
					'label' => 'Price Each',
					'slug' => 'price_each',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'default' => 3,
						),
				),
			'fld_730490' =>
				array(
					'ID' => 'fld_730490',
					'type' => 'hidden',
					'label' => 'Order Id',
					'slug' => 'order_id',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'default' => '',
						),
				),
			'fld_5786264' =>
				array(
					'ID' => 'fld_5786264',
					'type' => 'button',
					'label' => 'PAY',
					'slug' => 'pay',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'type' => 'submit',
							'class' => 'btn btn-default',
							'target' => '',
						),
				),
			'fld_9267447' =>
				array(
					'ID' => 'fld_9267447',
					'type' => 'text',
					'label' => 'Last Name',
					'slug' => 'last_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
		),
	'page_names' =>
		array(
			0 => 'Page 1',
		),
	'mailer' =>
		array(
			'on_insert' => 1,
			'sender_name' => 'Caldera Forms Notification',
			'sender_email' => 'admin@localhost.dev',
			'reply_to' => '',
			'email_type' => 'html',
			'recipients' => '',
			'bcc_to' => '',
			'email_subject' => 'A20',
			'email_message' => '{summary}',
		),
	'processors' =>
		array(
			'fp_97667047' =>
				array(
					'ID' => 'fp_97667047',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'increment_capture',
					'config' =>
						array(
							'start' => 1,
							'field' => 'fld_730490',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
			'fp_10004391' =>
				array(
					'ID' => 'fp_10004391',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'auth-net-single',
					'config' =>
						array(
							'auth-net-single-sandbox' => 'on',
							'auth-net-single-api_login' => 'CHANGE ME!',
							'auth-net-single-transaction_key' => 'CHANGE ME!',
							'auth-net-single-first_name' => '%first_name%',
							'auth-net-single-last_name' => '%last_name%',
							'auth-net-single-card_number' => '%credit_card_number%',
							'auth-net-single-card_cvc' => '%secret_code%',
							'auth-net-single-card_exp_month' => '%expiration_month%',
							'auth-net-single-card_exp_year' => '%expiration_year%',
							'auth-net-single-user_email' => 'fld_128749',
							'_required_bounds' =>
								array(
									0 => 'auth-net-single-user_email',
								),
							'auth-net-single-card_address' => '%address%',
							'auth-net-single-card_address_2' => '%address_line_two%',
							'auth-net-single-card_city' => '%city%',
							'auth-net-single-card_state' => '%state_province%',
							'auth-net-single-card_country' => '%country%',
							'auth-net-single-card_zip' => '%postal_code%',
							'auth-net-single-invoice_num' => '%order_id%',
							'auth-net-single-customer_id' => '%order_id%',
							'auth-net-single-price' => '%total%',
							'auth-net-single-description' => '',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
		),
	'conditional_groups' =>
		array(
			'_open_condition' => '',
		),
	'settings' =>
		array(
			'responsive' =>
				array(
					'break_point' => 'sm',
				),
		),
	'version' => '1.4.2-b1',
);