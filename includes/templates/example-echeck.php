<?php
return array(
	'_last_updated' => 'Sat, 20 Aug 2016 00:37:27 +0000',
	'ID' => 'authnet_echeck',
	'cf_version' => '1.4.2-b1',
	'name' => 'A20',
	'description' => '',
	'db_support' => 1,
	'pinned' => 0,
	'hide_form' => 1,
	'check_honey' => 1,
	'success' => 'Form has been successfully submitted. Thank you.',
	'avatar_field' => '',
	'form_ajax' => 1,
	'custom_callback' => '',
	'layout_grid' =>
		array(
			'fields' =>
				array(
					'fld_9668342' => '1:1',
					'fld_6816023' => '2:1',
					'fld_9267447' => '2:2',
					'fld_9756342' => '3:1',
					'fld_3367393' => '3:1',
					'fld_1050571' => '3:2',
					'fld_5917507' => '3:2',
					'fld_8906363' => '4:1',
					'fld_5786264' => '4:2',
				),
			'structure' => '12|6:6|6:6|6:6',
		),
	'fields' =>
		array(
			'fld_9668342' =>
				array(
					'ID' => 'fld_9668342',
					'type' => 'html',
					'label' => 'Signup Description',
					'slug' => 'signup_description',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'default' => '<h1>Make Online Payment Via Check</h1>
',
						),
				),
			'fld_6816023' =>
				array(
					'ID' => 'fld_6816023',
					'type' => 'text',
					'label' => 'Name',
					'slug' => 'name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9267447' =>
				array(
					'ID' => 'fld_9267447',
					'type' => 'text',
					'label' => 'Email',
					'slug' => 'email',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9756342' =>
				array(
					'ID' => 'fld_9756342',
					'type' => 'text',
					'label' => 'Routing Number ',
					'slug' => 'routing_number_',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => 'ABA routing transit number ',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_1050571' =>
				array(
					'ID' => 'fld_1050571',
					'type' => 'text',
					'label' => 'Bank Account Number',
					'slug' => 'bank_account_number',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => 'ABA Account number',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => 99,
							'type_override' => 'text',
						),
				),
			'fld_5786264' =>
				array(
					'ID' => 'fld_5786264',
					'type' => 'button',
					'label' => 'Pay',
					'slug' => 'pay',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'type' => 'submit',
							'class' => 'btn btn-default',
							'target' => '',
						),
				),
			'fld_3367393' =>
				array(
					'ID' => 'fld_3367393',
					'type' => 'text',
					'label' => 'Name On Account',
					'slug' => 'name_on_account',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_5917507' =>
				array(
					'ID' => 'fld_5917507',
					'type' => 'text',
					'label' => 'Bank Name',
					'slug' => 'bank_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_8906363' =>
				array(
					'ID' => 'fld_8906363',
					'type' => 'text',
					'label' => 'Amount To Pay',
					'slug' => 'amount_to_pay',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
		),
	'page_names' =>
		array(
			0 => 'Page 1',
		),
	'mailer' =>
		array(
			'on_insert' => 1,
			'sender_name' => 'Caldera Forms Notification',
			'sender_email' => 'admin@localhost.dev',
			'reply_to' => '',
			'email_type' => 'html',
			'recipients' => '',
			'bcc_to' => '',
			'email_subject' => 'A20',
			'email_message' => '{summary}',
		),
	'processors' =>
		array(
			'fp_97667047' =>
				array(
					'ID' => 'fp_97667047',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'increment_capture',
					'config' =>
						array(
							'start' => 1,
							'field' => '',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
			'fp_15228701' =>
				array(
					'ID' => 'fp_15228701',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'auth-net-echeck',
					'config' =>
						array(
							'auth-net-echeck-sandbox' => 'on',
							'auth-net-echeck-api_login' => 'CHANGE ME!',
							'auth-net-echeck-transaction_key' => 'CHANGE ME!',
							'auth-net-echeck-amount' => '%amount_to_pay%',
							'auth-net-echeck-bank_aba_code' => '%routing_number_%',
							'auth-net-echeck-bank_acct_num' => '%bank_account_number%',
							'auth-net-echeck-bank_name' => '%bank_name%',
							'auth-net-echeck-bank_acct_name' => '%name_on_account%',
							'auth-net-echeck-bank_acct_type' => 'CHECKING',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
		),
	'conditional_groups' =>
		array(
			'_open_condition' => '',
		),
	'settings' =>
		array(
			'responsive' =>
				array(
					'break_point' => 'sm',
				),
		),
	'version' => '1.4.2-b1',
);