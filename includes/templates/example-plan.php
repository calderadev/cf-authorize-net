<?php
return array(
	'_last_updated' => 'Fri, 19 Aug 2016 23:37:52 +0000',
	'ID' => 'cfauthnetplan',
	'cf_version' => '1.4.2-b1',
	'name' => 'A20',
	'description' => '',
	'db_support' => 1,
	'pinned' => 0,
	'hide_form' => 1,
	'check_honey' => 1,
	'success' => 'Form has been successfully submitted. Thank you.',
	'avatar_field' => '',
	'form_ajax' => 1,
	'custom_callback' => '',
	'layout_grid' =>
		array(
			'fields' =>
				array(
					'fld_9668342' => '1:1',
					'fld_6816023' => '2:1',
					'fld_9267447' => '2:2',
					'fld_128749' => '3:1',
					'fld_7758358' => '3:1',
					'fld_9516739' => '3:1',
					'fld_8804140' => '3:1',
					'fld_4498248' => '4:1',
					'fld_2198431' => '4:2',
					'fld_9387333' => '5:1',
					'fld_8436903' => '5:2',
					'fld_7740244' => '6:1',
					'fld_9756342' => '7:1',
					'fld_1050571' => '7:2',
					'fld_2715295' => '7:3',
					'fld_5786264' => '8:2',
				),
			'structure' => '12|6:6|12|6:6|6:6|12|4:4:4|6:6',
		),
	'fields' =>
		array(
			'fld_6816023' =>
				array(
					'ID' => 'fld_6816023',
					'type' => 'text',
					'label' => 'First Name',
					'slug' => 'first_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9267447' =>
				array(
					'ID' => 'fld_9267447',
					'type' => 'text',
					'label' => 'Last Name',
					'slug' => 'last_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_128749' =>
				array(
					'ID' => 'fld_128749',
					'type' => 'email',
					'label' => 'Email Address',
					'slug' => 'email_address',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
						),
				),
			'fld_7758358' =>
				array(
					'ID' => 'fld_7758358',
					'type' => 'text',
					'label' => 'Company',
					'slug' => 'company',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9516739' =>
				array(
					'ID' => 'fld_9516739',
					'type' => 'text',
					'label' => 'Address',
					'slug' => 'address',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_8804140' =>
				array(
					'ID' => 'fld_8804140',
					'type' => 'text',
					'label' => 'Address Line Two',
					'slug' => 'address_line_two',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9387333' =>
				array(
					'ID' => 'fld_9387333',
					'type' => 'text',
					'label' => 'Postal Code',
					'slug' => 'postal_code',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_8436903' =>
				array(
					'ID' => 'fld_8436903',
					'type' => 'dropdown',
					'label' => 'Country',
					'slug' => 'country',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'auto_type' => '',
							'taxonomy' => 'category',
							'post_type' => 'post',
							'value_field' => 'name',
							'orderby_tax' => 'name',
							'orderby_post' => 'name',
							'order' => 'ASC',
							'show_values' => 1,
							'default' => 'opt1192667',
							'option' =>
								array(
									'opt1192667' =>
										array(
											'value' => 'usa',
											'label' => 'United States',
										),
									'opt1677744' =>
										array(
											'value' => 'canada',
											'label' => 'Canada',
										),
								),
						),
				),
			'fld_7740244' =>
				array(
					'ID' => 'fld_7740244',
					'type' => 'text',
					'label' => 'Credit Card Number',
					'slug' => 'credit_card_number',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => 4007000000027,
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_9756342' =>
				array(
					'ID' => 'fld_9756342',
					'type' => 'text',
					'label' => 'Expiration Month',
					'slug' => 'expiration_month',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'masked' => 1,
							'mask' => 99,
							'type_override' => 'text',
						),
				),
			'fld_1050571' =>
				array(
					'ID' => 'fld_1050571',
					'type' => 'text',
					'label' => 'Expiration Year',
					'slug' => 'expiration_year',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'masked' => 1,
							'mask' => 99,
							'type_override' => 'text',
						),
				),
			'fld_2715295' =>
				array(
					'ID' => 'fld_2715295',
					'type' => 'text',
					'label' => 'Secret Code',
					'slug' => 'secret_code',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_5786264' =>
				array(
					'ID' => 'fld_5786264',
					'type' => 'button',
					'label' => 'Join',
					'slug' => 'join',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'type' => 'submit',
							'class' => 'btn btn-default',
							'target' => '',
						),
				),
			'fld_4498248' =>
				array(
					'ID' => 'fld_4498248',
					'type' => 'text',
					'label' => 'City',
					'slug' => 'city',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'mask' => '',
							'type_override' => 'text',
						),
				),
			'fld_2198431' =>
				array(
					'ID' => 'fld_2198431',
					'type' => 'states',
					'label' => 'State',
					'slug' => 'state',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
						),
				),
			'fld_9668342' =>
				array(
					'ID' => 'fld_9668342',
					'type' => 'html',
					'label' => 'Signup Description',
					'slug' => 'signup_description',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'default' => '<h1>Sign Up For Our Service</h1>
<p>You will be charge $25/month for 12 months.</p>',
						),
				),
		),
	'page_names' =>
		array(
			0 => 'Page 1',
		),
	'mailer' =>
		array(
			'on_insert' => 1,
			'sender_name' => 'Caldera Forms Notification',
			'sender_email' => 'admin@localhost.dev',
			'reply_to' => '',
			'email_type' => 'html',
			'recipients' => '',
			'bcc_to' => '',
			'email_subject' => 'A20',
			'email_message' => '{summary}',
		),
	'processors' =>
		array(
			'fp_97667047' =>
				array(
					'ID' => 'fp_97667047',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'increment_capture',
					'config' =>
						array(
							'start' => 1,
							'field' => '',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
			'fp_27409612' =>
				array(
					'ID' => 'fp_27409612',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'auth-net-plan',
					'config' =>
						array(
							'auth-net-rec-sandbox' => 'on',
							'auth-net-rec-api_login' => 'Change Me!',
							'auth-net-rec-transaction_key' => 'Change Me!',
							'auth-net-rec-first_name' => '%first_name%',
							'auth-net-rec-last_name' => '%last_name%',
							'auth-net-rec-card_number' => '%credit_card_number%',
							'auth-net-rec-card_cvc' => '%secret_code%',
							'auth-net-rec-card_exp_month' => '%expiration_month%',
							'auth-net-rec-card_exp_year' => '%expiration_year%',
							'auth-net-rec-user_email' => 'fld_128749',
							'_required_bounds' =>
								array(
									0 => 'auth-net-rec-user_email',
								),
							'auth-net-rec-card_address' => '%address%',
							'auth-net-rec-card_address_2' => '%address_line_two%',
							'auth-net-rec-card_city' => '%city%',
							'auth-net-rec-card_state' => '%state%',
							'auth-net-rec-card_country' => '%country%',
							'auth-net-rec-card_zip' => '%postal_code%',
							'auth-net-rec-invoice_num' => '%order_id%',
							'auth-net-rec-plan_name' => '1 Year Payment Plan',
							'auth-net-rec-billing-units' => 'months',
							'auth-net-rec-billing-interval' => 1,
							'auth-net-rec-occurrences' => 12,
							'auth-net-rec-trial-occurrences' => 0,
							'auth-net-rec-amount' => 25,
							'auth-net-rec-trials-amount' => 0,
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
		),
	'conditional_groups' =>
		array(
			'_open_condition' => '',
		),
	'settings' =>
		array(
			'responsive' =>
				array(
					'break_point' => 'sm',
				),
		),
	'version' => '1.4.2-b1',
);