<?php
/**
 * Processor config UI for Authorize.net for Caldera Forms version 1
 *
 * @package   cf_authorize-net
 * @author    Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com)
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 Josh Pollock for CalderaWP LLC
 */
if ( class_exists( 'Caldera_Forms_Processor_UI' ) ) {
	printf( '<div class="notice notice-error"><p>%s</p></div>', esc_html__( 'This processor is deprecated and will be removed soon. Please switch to the new version', 'cf-authorize-net' ) );
	echo Caldera_Forms_Processor_UI::ssl_notice( 'Authorize.net for Caldera Forms' );
	echo Caldera_Forms_Processor_UI::config_fields( cf_authorize_net_fields() );
}
