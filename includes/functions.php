<?php
/**
 * Functions for Authroize.net for Caldera Forms
 *
 * @package   cf_authorize-net
 * @author    Josh Pollock Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com)
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 Josh Pollock for CalderaWP LLC for CalderaWP LLC
 */

/**
 * Load our processors
 *
 * @since 2.0.0
 */
function cf_authorize_net_load(){
	$single = array(
		"name"				=>	__( 'Authorize.net Single Payment', 'cf-authorize-net'),
		"description"		=>	__( 'One time payments via Credit Cards', 'cf-authorize-net'),
		"icon"				=>	CF_AUTHORIZE_NET_URL . "icon.png",
		"author"			=>	'Caldera Labs',
		"author_url"		=>	'https://CalderaForms.com',
		"template"			=>	CF_AUTHORIZE_NET_PATH . "includes/config-single.php",
		"cf_ver"            =>  '1.3.6'
	);

	$eCheck = array(
		"name"				=>	__( 'Authorize.net eCheck Payment', 'cf-authorize-net'),
		"description"		=>	__( 'One time payments via eChecks', 'cf-authorize-net'),
		"icon"				=>	CF_AUTHORIZE_NET_URL . "icon.png",
		"author"			=>	'Caldera Labs',
		"author_url"		=>	'https://CalderaForms.com',
		"template"			=>	CF_AUTHORIZE_NET_PATH . "includes/config-echeck.php",
		"cf_ver"            =>  '1.3.6'
	);


	$plan_cc = array(
		"name"				=>	__( 'Authorize.net Payment Plans - Credit Card', 'cf-authorize-net'),
		"description"		=>	__( 'Create payment plans paid by credit card', 'cf-authorize-net'),
		"icon"				=>	CF_AUTHORIZE_NET_URL . "icon.png",
		"author"			=>	'Caldera Labs',
		"author_url"		=>	'https://CalderaForms.com',
		"template"			=>	CF_AUTHORIZE_NET_PATH . "includes/config-plan.php",
		"cf_ver"            =>  '1.3.6'
	);
	
	$plan_echeck = array(
		"name"				=>	__( 'Authorize.net Payment Plans - eCheck', 'cf-authorize-net'),
		"description"		=>	__( 'Create payment plans paid by eCheck', 'cf-authorize-net'),
		"icon"				=>	CF_AUTHORIZE_NET_URL . "icon.png",
		"author"			=>	'Caldera Labs',
		"author_url"		=>	'https://CalderaForms.com',
		"template"			=>	CF_AUTHORIZE_NET_PATH . "includes/config-plan-echeck.php",
		"cf_ver"            =>  '1.4.8'
	);
	

	cf_authorize_net_register_autoloader();
	
	new CF_Auth_Net_Single( $single, cf_authorize_net_fields_single(), 'auth-net-single' );
	new CF_Auth_Net_Plancc( $plan_cc, cf_authorize_net_fields_plan(), 'auth-net-plan' );
	new CF_Auth_Net_Planecheck( $plan_echeck, cf_authorize_net_echeck_fields_plan(), 'auth-new-plan-echeck' );
	new CF_Auth_Net_ECheck( $eCheck, cf_authorize_net_echeck_fields(), 'auth-net-echeck' );
	
}


/**
 * Register with CF Autoloaders
 *
 * @since 2.0.0
 */
function cf_authorize_net_register_autoloader(){

	Caldera_Forms_Autoloader::add_root( 'CF_Auth_Net', CF_AUTHORIZE_NET_PATH . 'classes' );
}



/**
 * Initializes the licensing system
 *
 * @uses "admin_init" action
 *
 * @since 0.1.0
 */
function cf_authorize_net_init_license(){

	$plugin = array(
		'name'		=>	'Authorize.net for Caldera Forms',
		'slug'		=>	'authorize-net-for-caldera-forms',
		'url'		=>	'https://calderawp.com/',
		'version'	=>	CF_AUTHORIZE_NET_VER,
		'key_store'	=>  'cf_authorize-net_license',
		'file'		=>  CF_AUTHORIZE_NET_CORE,
	);

	if( function_exists( 'caldera_warnings_dismissible_notice' ) ){
		new \calderawp\licensing_helper\licensing($plugin);
	}


}

/**
 * Get User IP
 *
 * Returns the IP address of the current visitor
 *
 * @since 1.0
 * @return string $ip User's IP address
 */
function cf_authorize_net_get_ip() {

	$ip = '127.0.0.1';

	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		//check ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		//to check ip is pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} elseif ( ! empty( $_SERVER['REMOTE_ADDR'] ) ) {
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;

}

/**
 * Add our example forms
 *
 * @uses "caldera_forms_get_form_templates"
 *
 * @since 0.1.0
 *
 * @param array $forms Example forms.
 *
 * @return array
 */
function  cf_authorize_net_example_form( $forms ) {
	$forms[ 'auth_net_1' ]	= array(
		'name'	=>	__( 'Authorize.net - One Time Payment - Variable Pricing', 'cf-authorize-net' ),
		'template'	=>	include CF_AUTHORIZE_NET_PATH . 'includes/templates/example-single.php'
	);

	$forms[ 'auth_net_2' ]	= array(
		'name'	=>	__( 'Authorize.net - Payment Plan', 'cf-authorize-net' ),
		'template'	=>	include CF_AUTHORIZE_NET_PATH . 'includes/templates/example-plan.php'
	);

	$forms[ 'auth_net_3' ]	= array(
		'name'	=>	__( 'Authorize.net - eCheck Payment', 'cf-authorize-net' ),
		'template'	=>	include CF_AUTHORIZE_NET_PATH . 'includes/templates/example-echeck.php'
	);

	$forms[ 'auth_net_4' ]	= array(
		'name'	=>	__( 'Authorize.net - Payment Plan eCheck', 'cf-authorize-net' ),
		'template'	=>	include CF_AUTHORIZE_NET_PATH . 'includes/templates/example-echeck.php'
	);

	return $forms;

}
